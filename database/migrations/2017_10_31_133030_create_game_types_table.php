<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gametypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('bggname');
            $table->timestamps();
        });

        // Insert the default user
        DB::table('gametypes')->insert(
            array(
                array(
                    'bggname' => 'boardgame',
                    'name' => 'Joc de taula',
                ),
                array(
                    'bggname' => 'rolegame',
                    'name' => 'Joc de rol',
                ),
                array(
                    'bggname' => 'cardgame',
                    'name' => 'Joc de cartes',
                ),
                array(
                    'bggname' => 'minigame',
                    'name' => 'Joc de miniatures',
                )
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gametypes');
    }
}
