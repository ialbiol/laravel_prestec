<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bggid')->unique();
            $table->integer('idType')->default(1);
            $table->string('name')->nullable();
            $table->string('bggurlthumbnail')->nullable();
            $table->string('bggurlimage')->nullable();
            $table->string('bggurl')->nullable();
            $table->text('description')->nullable();
            $table->string('year')->nullable();
            $table->integer('minplayers')->nullable();
            $table->integer('maxplayers')->nullable();
            $table->integer('minplaytime')->nullable();
            $table->integer('maxplaytime')->nullable();
            $table->integer('minage')->nullable();
            $table->integer('idStatus')->default(1);
            $table->integer('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
