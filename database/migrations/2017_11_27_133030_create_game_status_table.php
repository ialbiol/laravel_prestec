<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamestatus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });

        // Insert the default user
        DB::table('gamestatus')->insert(
            array(
                array(
                    'name' => 'Discusió compra',
                ),
                array(
                    'name' => 'Pendent de compra',
                ),
                array(
                    'name' => 'Disponible',
                ),
                array(
                    'name' => 'No disponible',
                )
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gamestatus');
    }
}
