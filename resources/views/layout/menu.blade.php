<li><a href="/newgame">New game</a></li>

<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
    Options <span class="caret"></span>
</a>

<ul class="dropdown-menu">
    <li><a href="/themes">Themes</a></li>
    <li><a href="/types">Types</a></li>
    <li><a href="/status">Status</a></li>
</ul>
</li>