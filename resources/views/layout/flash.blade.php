@if (Session::has('message'))
<div class="form-group">
    <div class="alert alert-danger">
        {{ Session::get('message') }}
    </div>
</div>
@endif