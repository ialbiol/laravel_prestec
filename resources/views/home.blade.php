@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Games <span style="float:right"><a href="/newgame">New Game</a></span></div>

                <div class="panel-body">
                    <ul class="game-list">
                    @foreach ($games as $game)
                        <li class="game-list-element">
                            <a href="/game/{{ $game->id }}" alt="{{ $game->name }}">
                                <div class="game-list-element-top">
                                    <img src="{{ $game->bggurlthumbnail }}" alt="{{ $game->name }}" />
                                </div>
                                <div class="game-list-element-bottom">
                            @if ($game->minplayers != '')
                                <div class="game-list-element-left">
                                    {{ $game->minplayers }} - {{ $game->maxplayers }} <span class="glyphicon-user"></span>
                                </div>
                            @endif
                            @if ($game->minplaytime != '')
                                <div class="game-list-element-right">
                                    {{ $game->minplaytime }} - {{ $game->maxplaytime }} <span class="glyphicon-hourglass"></span>
                                </div>
                                </div>
                            @endif
                            </a>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
