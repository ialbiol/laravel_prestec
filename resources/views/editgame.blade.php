@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New game</div>

                <div class="panel-body">
                    <form id="new-form" action="{{ route('newgame') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Game name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>


                        <div class="form-group">
                            <label for="type">Game type</label>
                            <select name="idType" class="form-control" id="type">
                                @foreach ($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="theme">Game theme</label>
                            <select name="idTheme" class="form-control" id="theme">
                                @foreach ($themes as $theme)
                                    <option value="{{ $theme->id }}">{{ $theme->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="year">Year</label>
                            <input type="text" class="form-control" id="year" name="year">
                        </div>

                        <div class="form-group">
                            <label for="desc">Description</label>
                            <input type="text" class="form-control" id="desc" name="description">
                        </div>

                        <div class="form-group">
                            <label for="img">Image url</label>
                            <input type="text" class="form-control" id="img" name="image">
                        </div>

                        <div class="form-group">
                            <label for="url">Game url</label>
                            <input type="text" class="form-control" id="url" name="url">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">New</button>
                        </div>
                        @include('layout.formerror')
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
