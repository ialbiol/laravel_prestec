@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @include('layout.flash')

            <div class="panel panel-default">
                <div class="panel-heading">{{ $game->name }} - {{ $game->type->name }}</div>

                <div class="panel-body">
                    <div class="game-info-img">
                        <img src="{{ $game->bggurlimage }}" />
                    </div>
                    <div class="game-info-right">
                        <table class="table">
                        @if ($game->minplayers != '')
                            <tr>
                                <td>Year</td>
                                <td>{{ $game->year }}</td>
                            </tr>
                        @endif
                        @if ($game->minplayers != '')
                            <tr>
                                <td>Players</td>
                                <td>{{ $game->minplayers }} - {{ $game->maxplayers }}</td>
                            </tr>
                        @endif
                        @if ($game->minplayers != '')
                            <tr>
                                <td>Play time</td>
                                <td>{{ $game->minplaytime }} - {{ $game->maxplaytime }}</td>
                            </tr>
                        @endif
                            <tr>
                                <td colspan=2>
                                    <a href="{{ $game->bggurl }}" target="_blank"> BGG link </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="game-info-bottom">
                        {{ $game->description }}
                    </div>


                    @if($isOnALoan)
                    <form id="new-form" action="{{ route('returnloan', $game->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Return loan</button>
                        </div>
                        @include('layout.formerror')
                    </form>

                    @else
                    <form id="new-form" action="{{ route('newloan', $game->id) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">New loan</button>
                        </div>
                        @include('layout.formerror')
                    </form>

                    @endif

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
