@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Types</div>

                <div class="panel-body">
                <ul>
                    @foreach ($types as $type)
                        <li>{{ $type->name }}</li>
                    @endforeach
                </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Add new</div>

                <div class="panel-body">
                    <form id="new-form" action="{{ route('types') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Type</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">New</button>
                        </div>
                        @include('layout.formerror')
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
