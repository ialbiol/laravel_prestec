@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New game</div>

                <div class="panel-body">
                    <form id="new-form" action="{{ route('newgame') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Game GBB id</label>
                            <input type="text" class="form-control" id="bggid" name="bggid">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">New</button>
                        </div>
                        @include('layout.formerror')
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
