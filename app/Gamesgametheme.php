<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gamesgametheme extends Model
{

    protected $fillable = ['idTheme', 'idGame', 'returned'];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function theme()
    {
        return $this->belongsTo(Gametheme::class);
    }
}
