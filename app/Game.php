<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Services\CustomCURL;
use Illuminate\Support\Facades\DB;

class Game extends Model
{
    protected $fillable = ['bggid', 'name', 'idType', 'bggurlthumbnail', 'bggurlimage', 'bggurl', 'description', 'year', 'minplayers', 'maxplayers', 'minplaytime', 'maxplaytime', 'minage', 'idStatus', 'deleted'];


    public function theme()
    {
        //la clau que busca de manera automatica es theme_id. sino, s'ha de posar
        return $this->hasMany(Gamesgametheme::class, 'idGame');
    }

    public function type()
    {
        //la clau que busca de manera automatica es type_id. sino, s'ha de posar
        return $this->belongsTo('App\Gametype', 'idType'); //Gametype::class);
    }

    public function loan()
    {
        //la clau que busca de manera automatica es loan_id. sino, s'ha de posar
        return $this->hasMany(Loan::class, 'idGame');
    }



    public static function importGame($values){
        //1- buscar per la api de bgg el joc
        $Curl = new CustomCURL;

        $result = $Curl->getXml('www.boardgamegeek.com/xmlapi2/thing?id='.$values['bggid']);

        //$xml = simplexml_load_string($result['serverBodyRaw']);
        $data = str_replace ("\t", '', $result['serverBodyRaw']);
        $data = str_replace ("\n", '', $data);
        
        $xml = new \SimpleXMLElement($data);

        //echo print_r ($xml->item->link[1], true); exit;
        //echo $xml->item->yearpublished['value'] ; exit;
        
        //2- treure les dades el xml de resposta
        //busco el id del typegame
        $typegameID = DB::table('gametypes')->where('bggname', $xml->item['type'])->value('id');
        if ($typegameID == '' ){
            //insert el nou typegame
            $typegameID = DB::table('gametypes')->insertGetId(
                ['name' => $xml->item['type'], 'bggname' => $xml->item['type']]
            );
        }

        //array de dades
        $game = array(
        'bggid' => $values['bggid'],
        'name' => $xml->item->name[0]['value'],
        'idType' => $typegameID,
        'bggurlthumbnail' => $xml->item->thumbnail,
        'bggurlimage' => $xml->item->image,
        'bggurl' => 'https://boardgamegeek.com/' . $xml->item['type'] . '/' . $values['bggid'],
        'description' => $xml->item->description,
        'year' => $xml->item->yearpublished['value'],
        'minplayers' => $xml->item->minplayers['value'],
        'maxplayers' => $xml->item->maxplayers['value'],
        'minplaytime' => $xml->item->minplaytime['value'],
        'maxplaytime' => $xml->item->maxplaytime['value'],
        'minage' => $xml->item->minage['value'],
        'idStatus' => '1',
        'deleted' => '0',
        );

        //3- crear el joc
        //Game::create($game);
        $gameID = DB::table('games')->insertGetId($game);

        //4- crear els temes que no estiguen i asignar els que toquen al joc
        foreach ($xml->item->link as $link) {
            if($link['type'] == 'boardgamecategory'){

                //busco el id del gametheme
                $themegameID = DB::table('gamethemes')->where('bggid', $link['id'])->value('id');
                if ($themegameID == '' ){
                    //insert el nou gametheme
                    $themegameID = DB::table('gamethemes')->insertGetId(
                        ['name' => $link['value'], 'bggid' => $link['id']]
                    );
                }

                //asigno el gametheme al joc
                $ID = DB::table('gamesgamethemes')->insertGetId(
                    ['idGame' => $gameID, 'idTheme' => $themegameID]
                );
            }
         }
    }

}
