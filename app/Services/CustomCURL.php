<?php

namespace App\Services;

class CustomCURL
{

    /**
     * customCURL description
     * @param  array $url  [url]
     * @return array        [description]
     */
    public function getXml($url)
    {
        $message = '';
        $body = '';
        $httpcode = '';

        $ch = curl_init();
        $options = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_AUTOREFERER    => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT        => 120,
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
        );
        curl_setopt_array( $ch, $options );

        $result = curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $index = strpos($result, "<?");        
        $header = substr($result, 0, $index);
        $body = substr($result, $index);

        if ($httpcode != 200) {
            $errors = 1;
            $message = "Webservice error: " . curl_error($ch);
        } else {
            // tot ok
            $errors = 0;
            $message = "Webservice 0k";
        }
        curl_close($ch);

        $returnarray = array(
            'errno' => $errors,
            'error' => $message,
            'httpcode' => $httpcode,
            'serverHeaderRaw' => $header,
            'serverBodyRaw' => $body,
            'serverResultIndex' => $index,
        );

        return $returnarray;
    }

}
