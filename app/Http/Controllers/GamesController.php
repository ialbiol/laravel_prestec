<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Game;
use App\Gametype;
use App\Gametheme;
class GamesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the game list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::all();

        return view('home', compact('games'));
    }

    /**
     * Show the new game form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$themes = Gametheme::all();
        //$types = Gametype::all();

        //return view('newgame', compact('themes', 'types'));
        return view('newgame');
    }


    /**
     * Creates new game.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //dd(request()->all());

        $this->validate(request(), [
            'bggid' => 'required',
        ]);

        Game::importGame(request(['bggid']));

        return redirect('home');
    }


    /**
     * Show the selected game.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($gameID)
    {
        $isOnALoan = false;

        $game = Game::find($gameID);

        //return $game;

        if($game->loan()->where('returned', '0')->count() > 0)
        {
            $isOnALoan = true;
        }

        return view('showgame', compact('game', 'isOnALoan'));
    }

}
