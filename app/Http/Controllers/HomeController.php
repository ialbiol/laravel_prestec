<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Game;
use App\Gametype;
use App\Gametheme;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::all();

        return view('home', compact('games'));
    }

}
