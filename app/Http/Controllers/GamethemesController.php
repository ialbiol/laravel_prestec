<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gametheme;

class GamethemesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the game themes list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themes = Gametheme::all();

        return view('themes', compact('themes'));
    }


    /**
     * Creates new game theme.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //dd(request()->all());

        $this->validate(request(), [
            'name' => 'required'
        ]);

        Gametheme::create(request(['name']));


        $themes = Gametheme::all();

        return view('themes', compact('themes'));
    }

}
