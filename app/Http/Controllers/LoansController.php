<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Session;

use App\Loan;
use App\Game;

class LoansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Create new loan.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $gameID)
    {
        Loan::create([
            'idUser' => Auth()->id(),
            'idGame' => $gameID,
            'returned' => 0
        ]);

        //return $game;
        //return $isOnALoan;

        $request->session()->flash('message', 'Loan accepted correctly');

        return back();//->withErrors([
        // 'message' => 'error text'
        //]);
    }

    /**
     * Returns a loan.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $gameID)
    {

        $game = Game::find($gameID);
        
        $loan = $game->loan()->where('returned', '0')->update(['returned' => 1]);
        
        //return $loan;

        if($loan)
        {
            //$loan->returned = 1;
            //$loan->save();
            $request->session()->flash('message', 'Loan returned correctly');
        } else {
            $request->session()->flash('message', 'ERROR: Loan not returned');
        }

        return back();
    }

}
