<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gametype;
class GametypesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the game types list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Gametype::all();

        return view('types', compact('types'));
    }


    /**
     * Creates new game type.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //dd(request()->all());

        $this->validate(request(), [
            'name' => 'required'
        ]);

        Gametype::create(request(['name']));


        $types = Gametype::all();

        return view('types', compact('types'));
    }

}
